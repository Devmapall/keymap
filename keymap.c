#include <stdio.h>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <stdlib.h>

int main(int argc, char **argv) {
	int fd;
	struct timeval tval_before, tval_after, tval_result;
	FILE *f = fopen("result.txt", "w");

	if(f == NULL) {
		printf("Error opening result.txt file!");
		exit(1);
	}

	//gettimeofday(&tval_before, NULL);

	fd = open("/dev/input/event0", O_RDONLY);
	struct input_event ev;

	while(ev.code != 74) {
		read(fd, &ev, sizeof(struct input_event));

		if(ev.code == 78 && ev.value == 1) {
			gettimeofday(&tval_before, NULL);
			printf("Timer start\n");
		} else {
			if(ev.type == 1 && ev.value == 1) {
				gettimeofday(&tval_after, NULL);
				timersub(&tval_after, &tval_before, &tval_result);
				printf("key %i Time: %ld.%06ld\n", ev.code, (long int)tval_result.tv_sec, (long int)tval_result.tv_usec);
				fprintf(f,"%i:%ld.%06ld\n", ev.code, (long int)tval_result.tv_sec, (long int)tval_result.tv_usec);
				fflush(f);
			}
		}
	}
	fclose(f);
	return 0;
}
